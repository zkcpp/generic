#include <iostream>
#include "Blockchain.h"
using namespace std;
Blockchain::Blockchain()
{
	this->data = nullptr;
	this->key = nullptr;
	this->hash = NULL;
	this->parent = nullptr;
}

Blockchain::Blockchain(std::string key, std::string data, int hash, std::string parent)
{
	this->key = key;
	this->data = data;
	this->hash = hash;
	this->parent = parent;
	this->dalsi = nullptr;
}

Blockchain::~Blockchain()
{
	delete this;
}

void Blockchain::setKey(std::string key)
{
	this->key = key;
}

void Blockchain::setData(std::string data)
{
	this->data = data;
}

void Blockchain::setHash(int hash)
{
	this->hash = hash;
}

void Blockchain::setParent(std::string parent)
{
	this->parent = parent;
}

int Blockchain::VypocitejHash()
{
	int prvni = 0x66;
	const char *SumData = data.c_str();
	const char* SumKey = key.c_str();
	const char* SumParentKey = parent.c_str();
	
	int parentHash = this->dalsi->getHash();
	int hashBloku = 0x66 +  (int)SumKey + (int)SumData +  (int)SumParentKey + (int)parentHash;
	return hashBloku;
}

Blockchain * Blockchain::getDalsi()
{
	if (dalsi != nullptr) {
		return dalsi;
	}
	else {
		return nullptr;
	}
	
}

void Blockchain::setDalsi(Blockchain * prvek)
{
	this->dalsi = prvek;
}

std::string Blockchain::getKey()
{
	return key;
}

std::string Blockchain::getData()
{
	return data;
}

int Blockchain::getHash()
{
	return hash;
}

std::string Blockchain::getParent()
{
	return parent;
}

void Blockchain::Vypis()
{
	cout << "'key':'" << key << "','data':" << data << "','hash':" << hash << "','parent':" << parent<< endl;
}
