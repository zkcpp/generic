#include <fstream>
#include <iostream>

#include "Seznam.h"
using namespace std;
Seznam::Seznam()
{
	char data[100];
	int pocet = 0;
	int cislo;
	// open a file in read mode.
	ifstream infile;
	infile.open("../data/index");
	if (infile.is_open()) {
	infile >> pocet;
	cout << pocet << endl;
	dataChain = new string[pocet];

	for (int i = 0; i < pocet; i++) {
		infile >> data;
		dataChain[i] = string(data);
	}
	//for (int i = 0; i < pocet; i++) {
	//	cout << dataChain[i] << endl;
	//}
	}
	infile.close();
	ifstream infile2;
	infile2.open("../data/000000");
	Blockchain* prvni = new Blockchain("","",0,"");
	if (infile2.is_open()) {
		prvni->setKey("000000");
		infile2 >> data;
		prvni->setData(string(data));
		infile2 >> cislo;
		prvni->setHash(cislo);
		infile2 >> data;
		prvni->setParent(data);
	}
	infile2.close();
	this->prvni = prvni;
	this->dalsi = prvni;
	this->nejdelsi = prvni;
	this->pocetNejdelsi = 1;
	this->pocetCelkem = pocet;
	this->pocetNeposkozenych = 1;
}

Seznam::~Seznam()
{
	delete this;
}

void Seznam::Vypis()
{
	Blockchain* prvek = prvni;
	while(prvek != nullptr){
		prvek->Vypis();
		prvek = prvek->getDalsi();
	}
	
}

void Seznam::Vybuduj()
{
	ifstream infile2;
	char data[100];
	int pocetNeposkozenych = 0;
	int cislo;
	Blockchain* prvek = this->prvni;
	for (int i = 0; i < this->pocetCelkem; i++) {

		infile2.open("../data/" + dataChain[i]);
		Blockchain* novy = new Blockchain("", "", 0, "");
		if (infile2.is_open()) {
			novy->setKey(dataChain[i]);
			infile2 >> data;
			novy->setData(string(data));
			infile2 >> cislo;
			novy->setHash(cislo);
			infile2 >> data;
			novy->setParent(data);
		}
		infile2.close();	
		prvek->setDalsi(novy);
		prvek = prvek->getDalsi();
	}
}

Blockchain * Seznam::getPrvni()
{
	return prvni;
}

void Seznam::Vloz(Blockchain * prvek)
{
}

void Seznam::setNejdelsi(Blockchain * prvek)
{
}

void Seznam::setPrvek(Blockchain * prvek)
{
}

Blockchain * Seznam::findParent(std::string parent)
{
		Blockchain* prvek = this->prvni;
		while (prvek->getDalsi() != nullptr) {
		if (prvek->getParent() == parent) {
			return prvek;
		}
		else {
			prvek = prvek->getDalsi();
		}
	}	
}
