#ifndef __BLOCKCHAIN_H
#define __BLOCKCHAIN_H
#include <string>
struct Blockchain {
private:
	std::string key;
	std::string data;
	int hash;
	std::string parent;
	Blockchain*dalsi;
public:
	Blockchain();
	Blockchain(std::string key, std::string data, int hash, std::string parent);
	~Blockchain();
	
	void setKey(std::string key);
	void setData(std::string data);
	void setHash(int hash);
	void setParent(std::string parent);
	
	int VypocitejHash();
	
	Blockchain* getDalsi();
	void setDalsi(Blockchain* prvek);
	
	std::string getKey();
	std::string getData();
	int	getHash();
	std::string getParent();
	
	void Vypis();
};
#endif //__BLOCKCHAIN_H
