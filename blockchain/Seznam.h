#ifndef __SEZNAM_H
#define __SEZNAM_H
#include "Blockchain.h"
//000000 cislo platne v c++
// hash je dan jako matfunkce, ASum je retezec (musi se prevest)
//ord c++ hodnota asci tabulky
struct Seznam {
private: 
	Blockchain* prvni;
	Blockchain* dalsi;
	Blockchain* nejdelsi;
	Blockchain* predchozi;
	std::string* dataChain;
	int pocetNejdelsi;
	int pocetCelkem;
	int pocetNeposkozenych;
public:
	Seznam();
	~Seznam();
	void Vypis();
	void Vybuduj();
	Blockchain* getPrvni();
	void Vloz(Blockchain* prvek);
	void setNejdelsi(Blockchain * prvek);
	void setPrvek(Blockchain* prvek);
	Blockchain* findParent(std::string parent);
};
#endif //__SEZNAM_H
