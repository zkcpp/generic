#pragma once
#include <string>
#include "zvire.h"
using namespace std;

struct Pes:public Zvire{
public:
	string rasa;
public:
	Pes();
	Pes(string rasa, int vyska, double vaha);
	void ulozit(ofstream&);
	Pes* nacti(ifstream&);
	void vypis();

};