// Jirman.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "zvire.h"
#include "pes.h"

#include <fstream>
#include <iostream>
#include <string>



void uloz() {
	Pes p1{ "Doga", 85, 33 };
	Pes p2{ "sarpej",25,10 };
	ofstream out("uloz.bin", ios::binary);
	Pes poleZ[2];
	poleZ[0] = p1;
	poleZ[1] = p2;

	for (size_t i = 0; i < 2; i++)
	{
		poleZ[i].ulozit(out);
	}
	out.close();
}

Pes* nacti() {
	Pes * psi = new Pes[2];
	ifstream in("uloz.bin", ios::binary);
	for (size_t i = 0; i < 2; i++)
	{
		psi[i].nacti(in);
		cout << psi[i].vypis;
	}
	in.close();

	return psi;
}


int main()
{
	uloz();
	nacti();
	return 0;
}

