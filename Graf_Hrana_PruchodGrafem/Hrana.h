#pragma once
#ifndef HRANA_H
#define HRANA_H

#include"Vrchol.h"

template<typename V, typename H>
class Vrchol;

template<typename V, typename H>
class Hrana
{
public:
	Hrana(H hodnota);
	~Hrana();
	H data;
	Vrchol<V, H>* v1;
	Vrchol<V, H>* v2;
	Vrchol<V, H>* dejOpacnouStranu(Vrchol<V, H>* v) const;
};

template<typename V, typename H>
inline Hrana<V, H>::Hrana(H hodnota)
{
	v1 = nullptr;
	v2 = nullptr;

	data = hodnota;
}

template<typename V, typename H>
inline Hrana<V, H>::~Hrana()
{
	delete v1;
	delete v2;
}
template<typename V, typename H>
inline Vrchol<V, H>* Hrana<V, H>::dejOpacnouStranu(Vrchol<V, H>* v) const
{
	if (v == v1)
	{
		return v2;
	}
	else if (v == v2)
	{
		return v1;
	}
	throw std::invalid_argument("Zadany vrchol neni pridruzeny teto hrane.");
};
#endif // !HRANA_H
