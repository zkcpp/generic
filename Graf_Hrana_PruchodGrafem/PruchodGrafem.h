#ifndef PRUCHOD_GRAFEM_H
#define PRUCHOD_GRAFEM_H

template<typename V, typename H>
class PruchodGrafem
{
public:
	static bool jeDosazitelny(Vrchol<V, H>* a, Vrchol<V, H>* b);
};

template<typename V, typename H>
inline bool PruchodGrafem<V, H>::jeDosazitelny(Vrchol<V, H>* a, Vrchol<V, H>* b)
{
	Vrchol<V, H>* zasobnik[LIMIT];
	Vrchol<V, H>* dosazeneVrcholy[LIMIT];

	for (size_t i = 0; i < LIMIT; i++)
	{
		zasobnik[i] = nullptr;
		dosazeneVrcholy[i] = nullptr;
	}
	int pocetVrcholuZasobnik = 0;

	zasobnik[pocetVrcholuZasobnik] = a;
	while (zasobnik[0] != nullptr)
	{
		Vrchol<V, H>* vrch = zasobnik[0];
		zasobnik[0] = nullptr;
		//pocetVrcholuZasobnik--;
		//for (int i = 0; i < 8; ++i)
		//{
		//	zasobnik[i] = zasobnik[i + 1];
		//}

		if (vrch == b)
		{
			return true;
		}
		else
		{
			bool jeDosazen = false;
			for (size_t i = 0; i < LIMIT; i++)
			{
				if (dosazeneVrcholy[i] == vrch)
				{
					jeDosazen = true;
					break;
				}
			}

			bool jeHotovo = false;
			if (!jeDosazen)
			{
				for (size_t i = 0; i < LIMIT; i++)
				{
					if (jeHotovo)
					{
						break;
					}
					if (dosazeneVrcholy[i] == nullptr)
					{
						dosazeneVrcholy[i] = vrch;

						for (size_t i = 0; i < LIMIT; i++)
						{
							try
							{
								if (vrch->hrany[i] != nullptr)
								{
									zasobnik[pocetVrcholuZasobnik++] = vrch->hrany[i]->dejOpacnouStranu(vrch);
								}
								else
								{
									jeHotovo = true;
									break;
								}
							}
							catch (const std::invalid_argument&)
							{

							}
						}

					}
				}
			}
		}
	}
	return false;
};
#endif // !PRUCHOD_GRAFEM_H


