#ifndef GRAF_H
#define GRAF_H

#include "Vrchol.h"
#include "Hrana.h"

class Graf
{
public:
	template<typename V, typename H>
	static void napojit(Vrchol<V, H>* a, Vrchol<V, H>* b, Hrana<V, H>* h);
};

template<typename V, typename H>
inline void Graf::napojit(Vrchol<V, H>* a, Vrchol<V, H>* b, Hrana<V, H>* h)
{
	h->v1 = a;
	h->v2 = b;

	for (size_t i = 0; i < LIMIT; i++)
	{
		if (a->hrany[i] == nullptr)
		{
			a->hrany[i] = h;
			break;
		}
	}
	for (size_t i = 0; i < LIMIT; i++)
	{
		if (b->hrany[i] == nullptr)
		{
			b->hrany[i] = h;
			break;
		}
	}
};
#endif // !GRAF_H
