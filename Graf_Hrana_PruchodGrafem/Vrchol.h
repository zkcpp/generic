#ifndef VRCHOL_H
#define VRCHOL_H

#define LIMIT 100
#include"Hrana.h"

template<typename V, typename H>
class Vrchol
{
public:
	Vrchol(V hodnota);
	~Vrchol();
	V data;
	Hrana<V, H>* hrany[LIMIT];
};

template<typename V, typename H>
inline Vrchol<V, H>::Vrchol(V hodnota)
{
	for (size_t i = 0; i < LIMIT; i++)
	{
		hrany[i] = nullptr;
	}
	data = hodnota;
}

template<typename V, typename H>
inline Vrchol<V, H>::~Vrchol()
{
	for (size_t i = 0; i < LIMIT; i++)
	{
		delete hrany[i];
	}
	delete[] hrany;
};
#endif // !VRCHOL_H
