#include "Graf.h"
#include "Hrana.h"
#include "Vrchol.h"
#include "PruchodGrafem.h"
#include <string>
#include <iostream>

using namespace std;

int main() {
	Vrchol<int, string>* v1 = new Vrchol<int, string>{ 1 };
	Vrchol<int, string>* v2 = new Vrchol<int, string>{ 2 };
	Vrchol<int, string>* v3 = new Vrchol<int, string>{ 3 };
	Vrchol<int, string>* v4 = new Vrchol<int, string>{ 4 };
	Vrchol<int, string>* v5 = new Vrchol<int, string>{ 5 };
	Vrchol<int, string>* v6 = new Vrchol<int, string>{ 6 };
	Vrchol<int, string>* v7 = new Vrchol<int, string>{ 7 };

	Graf::napojit(v1, v2, new Hrana<int, string>{ "h12" });
	Graf::napojit(v2, v3, new Hrana<int, string>{ "h23" });
	Graf::napojit(v3, v4, new Hrana<int, string>{ "h34" });
	Graf::napojit(v4, v5, new Hrana<int, string>{ "h45" });
	Graf::napojit(v1, v3, new Hrana<int, string>{ "h13" });

	Graf::napojit(v6, v7, new Hrana<int, string>{ "h67" });

	cout << (PruchodGrafem<int, string>::jeDosazitelny(v1, v5) ? "ano" : "chyba") << endl;
	cout << (PruchodGrafem<int, string>::jeDosazitelny(v1, v7) ? "chyba" : "ne") << endl;

	Graf::napojit(v4, v6, new Hrana<int, string>{ "h46" });

	cout << (PruchodGrafem<int, string>::jeDosazitelny(v1, v7) ? "ano" : "chyba") << endl;

	delete v1;
	delete v2;
	delete v3;
	delete v4;
	delete v5;
	delete v6;
	delete v7;

	system("pause");
	return 0;
}
