#ifndef _VZTAH_H
#define _VZTAH_H

#include "Osoba.h"


class Vztah {
public:
	Vztah(int pid, Osoba* pos1, Osoba* pos2, int pid_potomci,std::string ptypVztahu);
	~Vztah();
	int getId();
	int getIdPotomci();
	int getSizePotomci();
	Osoba** getOsoby();
	Osoba** getPotomci();
	std::string getTypVztahu();


	void addPotomek(Osoba* os);
private:
	int id;
	Osoba** osoba;
	Osoba** potomci;
	int size_potomci;
	int id_potomci;
	std::string typVztahu;
};

#endif // !_VZTAH_H
