#include "Vztah.h"

Vztah::Vztah(int pid, Osoba* pos1, Osoba* pos2, int pid_potomci, std::string ptypVztahu)
{
	id = pid;
	osoba = new Osoba*[2];
	potomci = new Osoba*[10];
	osoba[0] = pos1;
	osoba[1] = pos2;
	size_potomci = 0;
	id_potomci = pid_potomci;
	typVztahu = ptypVztahu;
}

Vztah::~Vztah()
{
	delete[] osoba;
}

int Vztah::getId()
{
	return id;
}

int Vztah::getIdPotomci()
{
	return id_potomci;
}

int Vztah::getSizePotomci()
{
	return size_potomci;
}

Osoba ** Vztah::getOsoby()
{
	return osoba;
}

Osoba ** Vztah::getPotomci()
{
	return potomci;
}

std::string Vztah::getTypVztahu()
{
	return typVztahu;
}

void Vztah::addPotomek(Osoba * os)
{
	if (os != nullptr)
	{
		potomci[size_potomci++] = os;
	}
}
