#ifndef _OSOBA_H
#define _OSOBA_H
#include "User_exception.h"
//#include "Vztah.h"

class Vztah;
class Osoba {
public:
	Osoba(int pId, std::string pJmeno, char pPohlavi);
	void pridejVztah(Vztah* pVztah);

	 int getId();
	 std::string getJmeno();
	 char getPohlavi();
	 Vztah** getVztahy();

	 int getSizeVztahy();

private:
	int id;
	std::string jmeno;
	char pohlavi;
	Vztah** vztahy;

	int current_size_vztahy;

	
};

#endif // !_OSOBA_H
