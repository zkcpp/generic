#ifndef _USER_EXCEPTION_H
#define _USER_EXCEPTION_H

#include <exception>
#include <string>

class User_exception : std::exception
{
public:
	User_exception(std::string message)
	{
		_message = message;
	}
	const char * what() const throw ()
	{
		return _message.c_str();
	}
private:
	std::string _message;
};

#endif
