#include "Osoba.h"

Osoba::Osoba(int pId, std::string pJmeno, char pPohlavi)
{
	id = pId;
	jmeno = pJmeno;
	pohlavi = pPohlavi;
	vztahy = new Vztah*[20];
 
	current_size_vztahy = 0;
}

void Osoba::pridejVztah(Vztah* pVztah)
{
	if (pVztah != nullptr)
	{
		vztahy[current_size_vztahy++] = pVztah;
	}
	throw User_exception("PridejVztah: pVztah is null");
}

int Osoba::getId()
{
	return id;
}

std::string Osoba::getJmeno()
{
	return jmeno;
}

char Osoba::getPohlavi()
{
	return pohlavi;
}

Vztah ** Osoba::getVztahy()
{
	return vztahy;
}

int Osoba::getSizeVztahy()
{
	return current_size_vztahy;
}
