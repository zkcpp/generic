#ifndef _POTOMEK_H
#define _POTOMEK_H

class Potomek {
public:
	Potomek(int pid_vztahu, int pid_potomek);
	int getIdVztahu();
	int getIdPotomek();
private:
	int id_vztahu;
	int id_potomek;
};
#endif
