#include "Potomek.h"

Potomek::Potomek(int pid_vztahu, int pid_potomek)
{
	id_vztahu = pid_vztahu;
	id_potomek = pid_potomek;
}

int Potomek::getIdVztahu()
{
	return id_vztahu;
}

int Potomek::getIdPotomek()
{
	return id_potomek;
}
