#ifndef _RODOKMEN_H
#define _RODOKMEN_H

#include "Osoba.h"
#include "Potomek.h"
#include "Vztah.h"
#include <fstream>


class Rodokmen {
public:
	Rodokmen(std::istream&);
	~Rodokmen();
	int dejPocetPrimychPotomku(int idOsoby);
	int dejPocetVsechPotomku(int idOsoby);

	int getSizeOsoby();
private:
	Osoba** osoby;
	Potomek** potomci;
	Vztah** vztahy;
	int current_size_osoby;
	int current_size_potomci;
	int current_size_vztahy;

	Osoba* find_person(int id);
	Osoba** find_potomci();

};

#endif // !1
