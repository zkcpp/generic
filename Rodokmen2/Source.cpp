#include "Rodokmen.h"
#include <iostream>

int main() {
	std::ifstream inFile("rodokmen.txt");	
	Rodokmen rodokmen(inFile);
	int size_osoby = rodokmen.getSizeOsoby();	
	for (int i = 0; i < size_osoby; i++)
	{
		int pocet_primych_potomku = rodokmen.dejPocetPrimychPotomku(i);
		std::cout << "idOsoby: " << i << " pocetPrimychPotomku: " << pocet_primych_potomku << std::endl;
	}


	return 0;
}
