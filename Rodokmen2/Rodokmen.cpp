#include "Rodokmen.h"
#include <iostream>

Rodokmen::Rodokmen(std::istream & stream)
{
	potomci = new Potomek*[20];
	vztahy = new Vztah*[20];
	osoby = new Osoba*[25];


	current_size_osoby = 0;
	current_size_potomci = 0;
	current_size_vztahy = 0;

	std::string buffer;
	
	
	while (!stream.eof()) 
	{
		stream >> buffer;
		if (buffer == "OSOBA")
		{
			int id;
			std::string jmeno;
			char pohlavi;
			stream >> id;
			stream >> pohlavi;
			stream >> jmeno;
			osoby[current_size_osoby++] = new Osoba(id, jmeno, pohlavi);
			
		}
		else if (buffer == "VZTAH")
		{			
			int id_os1;
			int id_os2;
			int id_potomci;
			std::string typVztahu;
			stream >> id_os1;
			stream >> id_os2;
			stream >> id_potomci;
			stream >> typVztahu;
			try
			{
				Osoba* os1 = find_person(id_os1);
				Osoba* os2 = find_person(id_os2);
				vztahy[current_size_vztahy++] = new Vztah(0, os1, os2, id_potomci, typVztahu);
			}
			catch (const User_exception& ex)
			{
				std::cerr << ex.what();
			}
			
			
			
		}
		else //POTOMEK
		{
			int id_vztahu;
			int id_potomek;
			stream >> id_vztahu;
			stream >> id_potomek;
			potomci[current_size_potomci++] = new Potomek(id_vztahu, id_potomek);

		}
	}

		for (int i = 0; i < current_size_vztahy; i++)//Vztahy
		{
			int id_potomci = vztahy[i]->getIdPotomci();
			auto posoby = vztahy[i]->getOsoby();
			for (int j = 0; j < current_size_potomci; j++)//potomci 
			{
				int id_vztahu = potomci[j]->getIdVztahu();
				if (id_potomci == id_vztahu)
				{
					int id_osoba = potomci[j]->getIdPotomek();

					Osoba* potomek = find_person(id_osoba);
					try
					{
						vztahy[i]->addPotomek(potomek);
					}
					catch (const User_exception& ex)
					{
						std::cerr << ex.what();
					}

				}

			}
			try
			{
				posoby[0]->pridejVztah(vztahy[i]);
				posoby[1]->pridejVztah(vztahy[i]);
			}
			catch (const User_exception& ex)
			{
				std::cerr << ex.what() << std::endl;
			}
			
		}

	

}

Rodokmen::~Rodokmen()
{
	for (int i = 0; i < current_size_osoby; i++)
		delete osoby[i];
	delete[] osoby;

	for (int i = 0; i < current_size_potomci; i++)
		delete potomci[i];
	delete[] potomci;

	for (int i = 0; i < current_size_vztahy; i++)
		delete vztahy[i];
	delete[] vztahy;


}

int Rodokmen::dejPocetPrimychPotomku(int idOsoby)
{
	try 
	{
		Osoba* os = find_person(idOsoby);
		auto vztahy = os->getVztahy();
		auto size_vztahy = os->getSizeVztahy();
		int pocet_primich_potomku = 0;
		for (int i = 0; i < size_vztahy; i++)
		{
			auto typVztahu = vztahy[i]->getTypVztahu();
		    pocet_primich_potomku += vztahy[i]->getSizePotomci();			
		}
		return pocet_primich_potomku;
	}
	catch (const User_exception& ex)
	{
		std::cerr << ex.what();
	}


}

int Rodokmen::dejPocetVsechPotomku(int idOsoby)
{
	return 0;
}

int Rodokmen::getSizeOsoby()
{
	return current_size_osoby;
}

Osoba * Rodokmen::find_person(int id)
{
	for (int i = 0; i < current_size_osoby; i++)
	{
		if (osoby[i]->getId() == id)
		{
			return osoby[i];
		}
	}
	throw User_exception("find_person: person not found");
}

Osoba ** Rodokmen::find_potomci()
{
	return nullptr;
}
