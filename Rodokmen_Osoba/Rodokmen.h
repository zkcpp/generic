#ifndef __RODOKMEN_H
#define __RODOKMEN_H
#include "Osoba.h"
#include <iostream>
struct Rodokmen {
private:
	Osoba** osoby;
	int pocetOsob;
public:
	Rodokmen(std::istream& is);
	~Rodokmen();
	void pridejOsobu(Osoba* o);
	int dejPocetPrimychPotomku(int idOsoby);
	int dejPocetVsechPotomku(int idOsoby);
	Osoba* najdiOsobu(int id);
};

#endif