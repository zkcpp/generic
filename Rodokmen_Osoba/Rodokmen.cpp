#include "Rodokmen.h"
#include <iostream>
#include <fstream>
#include <sstream>
#include <exception>
Rodokmen::Rodokmen(std::istream& is)
{
	osoby = nullptr;
	std::string str;
	int id;
	while (getline(is, str, ' ')) {
		if (!str.compare("OSOBA")) {
			Osoba* o = new Osoba{};
			getline(is, str, ' ');
			o->setId(stoi(str));
			getline(is, str, ' ');
			o->setPohlavi(str[0]);
			getline(is, str, '\n');
			o->setJmeno(str);
			pridejOsobu(o);
		}
		else if (!str.compare("VZTAH")) {
			getline(is, str, ' ');
			Osoba* o1 = najdiOsobu(stoi(str));
			getline(is, str, ' ');
			Osoba* o2 = najdiOsobu(stoi(str));
			getline(is, str, ' ');
			id = stoi(str);
			getline(is, str, ' ');
			o1->pridejVztah(stoi(str), str, o2);
			o2->pridejVztah(stoi(str), str, o1);
			getline(is, str, '\n');
		}
		else if (!str.substr(0, str.find(' ')).compare("POTOMEK")) {
			std::cout << "POTOMEK";
		}
		else {
			throw std::exception("CHYBA - neocekavany vstup");
		}
	}


}

int Rodokmen::dejPocetPrimychPotomku(int idOsoby)
{
	return 0;
}

Rodokmen::~Rodokmen()
{
}

int Rodokmen::dejPocetVsechPotomku(int idOsoby)
{
	return 0;
}

Osoba* Rodokmen::najdiOsobu(int id)
{
	for (int i = 0; i < pocetOsob; i++) {
		if (osoby[i]->getId() == id) {
			return osoby[i];
		}
	}
	throw std::exception("CHYBA - nebyla nalezena osoba se zadanym id");
}

void Rodokmen::pridejOsobu(Osoba* o)
{
	if (osoby == nullptr) {
		osoby = new Osoba*[1];
		pocetOsob = 0;
		osoby[pocetOsob++] = o;
		
	}
	else {
		Osoba** temp = new Osoba * [pocetOsob];
		for (int i = 0; i < pocetOsob; i++) {
			temp[i] = osoby[i];
		}
		delete[] osoby;
		osoby = temp;
		osoby[pocetOsob++] = o;
	}
}
