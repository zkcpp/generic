#ifndef __OSOBA_H
#define __OSOBA_H
#include <string>
struct Osoba {
private:
	int id;
	std::string jmeno;
	char pohlavi;
	struct Vztah {
	private:
		int id;
		Osoba* osoby = new Osoba[2];
		Osoba** potomci;
		std::string typVztahu;
	public:
		Vztah();
		~Vztah();
		void setId(int id);
		void setTypVztahu(std::string typ);
	};
	Vztah** vztahy;
	int pocetVztahu;
public:
	Osoba();
	~Osoba();
	void setId(int id);
	void setJmeno(std::string jmeno);
	void setPohlavi(char pohlavi);
	int getPocetVztahu() const;
	int getId() const;
	std::string getJmeno() const;
	char getPohlavi() const;
	Vztah** getVztahy() const;
	void pridejVztah(int id, std::string typVztahu, Osoba* o1);
};
#endif