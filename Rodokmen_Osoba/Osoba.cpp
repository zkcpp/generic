#include "Osoba.h"

Osoba::Osoba()
{
	vztahy = nullptr;
	pocetVztahu = 0;
}

Osoba::~Osoba()
{
}

void Osoba::setId(int id)
{
	this->id = id;
}

void Osoba::setJmeno(std::string jmeno)
{
	this->jmeno = jmeno;
}

void Osoba::setPohlavi(char pohlavi)
{
	this->pohlavi = pohlavi;
}

int Osoba::getPocetVztahu() const
{
	return pocetVztahu;
}

int Osoba::getId() const
{
	return id;
}

std::string Osoba::getJmeno() const
{
	return jmeno;
}

char Osoba::getPohlavi() const
{
	return pohlavi;
}

void Osoba::pridejVztah(int id, std::string typVztahu, Osoba* o1)
{
	if (vztahy == nullptr) {
		vztahy = new Vztah*[pocetVztahu++];
	}
	else {
		Vztah** temp = new Vztah*[pocetVztahu];
		for (int i = 0; i < pocetVztahu; i++) {
			temp[i] = vztahy[i];
		}
		delete vztahy;
		vztahy = temp;
		pridejVztah(id, typVztahu, o1);
	}
}

Osoba::Vztah::Vztah()
{
	potomci = nullptr;
	typVztahu = "";
	id = 0;
}

Osoba::Vztah::~Vztah()
{
	delete[] osoby;
	delete[] potomci;
}

void Osoba::Vztah::setId(int id)
{
	this->id = id;
}

void Osoba::Vztah::setTypVztahu(std::string typ)
{
	this->typVztahu = typ;
}
