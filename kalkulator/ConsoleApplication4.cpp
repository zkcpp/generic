#include <iostream>
#include "Kalkulator.h"

#define _CRTDBG_MAP_ALLOC
using namespace std;
int main()
{
	Kalkulator<double, 2> kalkulator{};
	kalkulator.pridejHodnotu(1.22);
	kalkulator.pridejOperator('+');
	kalkulator.pridejHodnotu(2.0);
	kalkulator.pridejOperator('*');
	kalkulator.pridejHodnotu(4.0);
	kalkulator.pridejOperator('/');
	kalkulator.pridejHodnotu(6.0);
	kalkulator.pridejOperator('-');
	kalkulator.pridejHodnotu(0.11);

	try {
		cout << kalkulator.vypocitej() << endl;
	}
	catch (const std::exception& e) {
		cout << e.what()<< endl;
	}

	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);
	return 0;
}
