#ifndef __OPERATOR_H
#define __OPERATOR_H

#include "PrvekVyrazu.h"
class Operator : public PrvekVyrazu {
private:
	char op;
public:
	Operator();
	Operator(const char& op);
	char getOperator() const;
	virtual bool jeOperator() const override;
	virtual bool jeHodnota() const override;
};
#endif


