#ifndef __HODNOTA_H
#define __HODNOTA_H
#include "Operator.h"
template <typename Data>
class Hodnota : public PrvekVyrazu {
private:
	Data hodnota;
public:
	Hodnota();
	Hodnota(const Data& hod);
	Data getHodnota() const;
	virtual bool jeOperator() const override;
	virtual bool jeHodnota() const override;
};
#endif

template<typename Data>
inline Hodnota<Data>::Hodnota()
{
	hodnota = nullptr;
}

template<typename Data>
inline Hodnota<Data>::Hodnota(const Data& hod)
{
	if (hod == NULL) {
		throw std::exception("CHYBA - nullptr hodnota");
	}
	else {
		hodnota = hod;
	}

}


template<typename Data>
inline Data Hodnota<Data>::getHodnota() const
{
	return hodnota;
}
template<typename Data>
bool Hodnota<Data>::jeOperator() const {
	return false;
}
template<typename Data>
bool Hodnota<Data>::jeHodnota() const {
	return true;
}