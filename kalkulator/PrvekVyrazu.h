#ifndef __PRVEKVYRAZU_H
#define __PRVEKVYRAZU_H
class PrvekVyrazu {
private:

public:
	PrvekVyrazu() {}
	virtual ~PrvekVyrazu() {}
	virtual bool jeOperator() const = 0;
	virtual bool jeHodnota() const = 0;
};

#endif