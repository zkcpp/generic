#ifndef __KALKULATOR_H
#define __KALKULATOR_H
#include <string>
#include <math.h> 
#include <iostream>
#include "Hodnota.h"
#include "Operator.h"
template <typename Data, int zaokrouhleni>
class Kalkulator {
public:
	Kalkulator();
	~Kalkulator();
	void pridejHodnotu(const Data& hodnota);
	void pridejOperator(const char& oper);
	std::string vypocitej();

private:
	PrvekVyrazu** prvky;
	int pocetPrvku;
	int velikostPole;
	double zaokrouhleni;
};

#endif

template<typename Data, int zaokrouhleni>
inline Kalkulator<Data, zaokrouhleni>::Kalkulator()
{
	velikostPole = 10;
	pocetPrvku = 0;
	this->zaokrouhleni = zaokrouhleni;
	prvky = new PrvekVyrazu*[velikostPole];
}

template<typename Data, int zaokrouhleni>
inline Kalkulator<Data, zaokrouhleni>::~Kalkulator()
{
	for (int i = 0; i < pocetPrvku; i++) {
		delete prvky[i];
	}
	delete[] prvky;
}

template<typename Data, int zaokrouhleni>
inline void Kalkulator<Data, zaokrouhleni>::pridejHodnotu(const Data& hodnota)
{
	if (pocetPrvku >= velikostPole) {
		PrvekVyrazu** temp = new PrvekVyrazu*[velikostPole + 10];
		for (int i = 0; i < pocetPrvku; i++) {
			temp[i] = prvky[i];
		}
		delete[] prvky;
		prvky = nullptr;
		prvky = temp;
		velikostPole += 10;
	}
	prvky[pocetPrvku++] = new Hodnota<Data>{ hodnota };

}

template<typename Data, int zaokrouhleni>
inline void Kalkulator<Data, zaokrouhleni>::pridejOperator(const char& oper)
{
	if (pocetPrvku >= velikostPole) {
		PrvekVyrazu** temp = new PrvekVyrazu*[velikostPole + 10];
		for (int i = 0; i < pocetPrvku; i++) {
			temp[i] = prvky[i];
		}
		delete[] prvky;
		prvky = nullptr;
		prvky = temp;
	}

	prvky[pocetPrvku++] = new Operator{ oper };
}

template<typename Data, int zaokrouhleni>
inline std::string Kalkulator<Data, zaokrouhleni>::vypocitej()
{
	PrvekVyrazu* prvek;
	char op;
	double vysledek = (Data)dynamic_cast<Hodnota<Data>*>(prvky[0])->getHodnota();
	for (int i = 1; i < pocetPrvku; i++) {
		if (prvky[i]->jeHodnota()) {
			if (i < pocetPrvku) {
				if (prvky[i - 1]->jeOperator()) {
					op = dynamic_cast<Operator*>(prvky[i - 1])->getOperator();
					switch (op) {
					case '+':
						vysledek += (Data)dynamic_cast<Hodnota<Data>*>(prvky[i])->getHodnota();
						break;
					case '-':
						vysledek -= (Data)dynamic_cast<Hodnota<Data>*>(prvky[i])->getHodnota();
						break;
					case '/':
						vysledek /= (Data)dynamic_cast<Hodnota<Data>*>(prvky[i])->getHodnota();
						break;
					case '*':
						vysledek *= (Data)dynamic_cast<Hodnota<Data>*>(prvky[i])->getHodnota();
						break;

					}
				}
				else {
					throw std::exception("CHYBA - po cisle musi nasledovat operator");
				}

			}

		}	
	}
	double nasobek = pow(10, zaokrouhleni);
	vysledek = round(vysledek * nasobek) / nasobek;
	return std::to_string(vysledek);
}
