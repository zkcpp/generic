#ifndef __ZVIRE_H
#define __ZVIRE_H
#include "Krmelec.h"
struct Zvire {
protected:
	int _energie = 10;
public:
	Zvire() {}
	Zvire(int energie) {}
	virtual ~Zvire() {}
	virtual void nakrmit(Krmelec& krmelec) = 0; //�sp�n� krmen� obnovi energii na 10
	virtual void vydejZvuk() = 0; //stoj� 2 jednotky energie.
	virtual bool zije() = 0; //informuje, zdali je zv���tko na�ivu (_energie > 0)
};

#endif