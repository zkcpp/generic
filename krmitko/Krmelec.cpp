#include "Krmelec.h"
#include <exception>
Krmelec::Krmelec(int kusuSena, int kusuGranuli, int kusuZeleniny) : _kusuSena(kusuSena), _kusuGranuli(kusuGranuli), _kusuZeleniny(kusuZeleniny)
{
}

Krmelec::~Krmelec()
{
}

Krmelec::Krmelec()
{
}

bool Krmelec::sezer(Krmivo k, int mnozstvi)
{
	switch (k) {
	case Seno:
		if (mnozstvi <= _kusuSena) {
			_kusuSena -= mnozstvi;
			return true;
		}
		return false;
		break;
	case Granule:
		if (mnozstvi <= _kusuGranuli) {
			_kusuGranuli -= mnozstvi;
			return true;
		}
		return false;
		break;
	case Zelenina:
		if (mnozstvi <= _kusuZeleniny) {
			_kusuZeleniny -= mnozstvi;
			return true;
		}
		return false;
		break;
	default:
		throw std::exception("CHYBA - neocekavany typ krmiva");
		break;
	}
}

void Krmelec::dosypej(Krmivo krmivo, int mnozstvi)
{
	switch (krmivo) {
	case Seno:
		_kusuSena += mnozstvi;
		break;
	case Granule:
		_kusuGranuli += mnozstvi;
		break;
	case Zelenina:
		_kusuZeleniny += mnozstvi;
		break;
	default:
		throw std::exception("CHYBA - neocekavany typ krmiva");
		break;
	}
}
