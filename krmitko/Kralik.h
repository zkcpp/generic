#ifndef __KRALIK_H
#define __KRALIK_H
#include "Zvire.h"
struct Kralik : Zvire {
private:
public:
	Kralik();
	Kralik(int energie);
	~Kralik();
	virtual void nakrmit(Krmelec& krmelec) override; //�sp�n� krmen� obnovi energii na 10
	virtual void vydejZvuk() override; //stoj� 2 jednotky energie.
	virtual bool zije() override; //informuje, zdali je zv���tko na�ivu (_energie > 0)
};
#endif