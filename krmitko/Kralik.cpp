#include "Kralik.h"
#include <iostream>
Kralik::Kralik() : Zvire(10)
{
}

Kralik::Kralik(int energie) :Zvire(energie)
{
}

Kralik::~Kralik()
{
}



void Kralik::nakrmit(Krmelec& krmelec)
{

	if (!krmelec.sezer(Seno, 10)) {
		std::cout << "Nedostatecny pocet sena, zkusim mu dat zeleninu..." << std::endl;
		if (krmelec.sezer(Zelenina, 2)) {
			std::cout << "Kralik narmen zeleninou" << std::endl;
		}
		else {
			std::cout << "Neni uz ani zelenina, je treba dosypat..." << std::endl;
		}
	}
	else {
		std::cout << "Kralik nakrmen senem" << std::endl;
	}
}

void Kralik::vydejZvuk()
{
	int flip = rand() % (10 - 3 + 1) + 1;
	if (flip <= 3) {
		Zvire::_energie -= 2;
		std::cout << "bzzzzm" << std::endl;
	}
}

bool Kralik::zije()
{
	if (Zvire::_energie > 0) {
		return true;
	}
	return false;
}
