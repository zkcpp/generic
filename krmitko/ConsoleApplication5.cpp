#include "Kralik.h"
#include <string>
#include <iostream>
#include <exception>
#define _CRTDBG_MAP_ALLOC
#define nahodne noexcept (rand() % (15 - 3 + 1)) + 3
using namespace std;
int main()
{
    Zvire** poleZvirat = new Zvire*[nahodne];
    for (int i = 0; i < nahodne; i++) {
        poleZvirat[i] = dynamic_cast<Zvire*>(new Kralik{});
    }
    Krmelec krmelec{50,100,10};
    std::string pom;
    bool zije = true;
    while (zije) {
        cout << "1-krm\n" << "2-dosypej\n" << "3-vydej zvuk\n" << endl << "Vas vyber:";
        cin >> pom;
        if (pom.size() == 1) {
            switch (pom[0]) {
            case '1':
                for (int i = 0; i < nahodne; i++) {
                    poleZvirat[i]->nakrmit(krmelec);
                    if (!poleZvirat[i]->zije()) {
                        zije = false;
                        break;
                    }
                }
                break;
            case '2':
                cout << "\n1-dosypej Seno\n" << "2-dosypej granule\n" << "3-dosypej zeleninu\n" << endl << "Vas vyber:";
                cin >> pom;
                if (pom.size() == 1) {
                    switch (pom[0]) {
                    case '1':
                        cout << "\nZadejte mnozstvi sena k dosypani:";
                        cin >> pom;
                        krmelec.dosypej(Seno, stoi(pom));
                        cout << "\nSeno bylo dosypano\n";
                        break;
                    case '2':
                        cout << "\nZadejte mnozstvi granuli k dosypani:";
                        cin >> pom;
                        krmelec.dosypej(Granule, stoi(pom));
                        cout << "\Granule byly dosypany\n";
                        break;
                    case '3':
                        cout << "\nZadejte mnozstvi zeleniny k dosypani:";
                        cin >> pom;
                        krmelec.dosypej(Zelenina, stoi(pom));
                        cout << "\Zelenina byla dosypana\n";
                        break;
                    default:
                        cout << "CHYBA - musite zadat pouze cisla z vyberu\n";         
                        break;
                    }
                    
                }
                else {
                    cout << "CHYBA - musite zadat pouze cisla z vyberu\n";
                    break;
                }
                break;
            case '3':
                for (int i = 0; i < nahodne; i++) {
                    poleZvirat[i]->vydejZvuk();
                    if (!poleZvirat[i]->zije()) {
                        zije = false;
                        cout << "Zviratko umrelo a ostatni se rozutekla" << endl;
                        break;
                    }
                }
                break;
            default:
                cout << "CHYBA - musite zadat pouze cisla z vyberu\n";
                break;
            }
        }
        else {
            cout << "CHYBA - musite zadat pouze cisla z vyberu\n";
            break;
        }
    }
    for (int i = 0; i < nahodne; i++) {
        delete poleZvirat[i];
    }
    delete[] poleZvirat;
    _CrtDumpMemoryLeaks();
    return 0;
}
