#ifndef __KRMELEC_H
#define __KRMELEC_H
#include "EnumKrmivo.h"
struct Krmelec {
private:
	int _kusuSena;
	int _kusuGranuli;
	int _kusuZeleniny;
public:
	Krmelec(int kusuSena, int kusuGranuli, int kusuZeleniny);
	~Krmelec();
	Krmelec();
	bool sezer(Krmivo k, int mnozstvi); 
	void dosypej(Krmivo krmivo, int mnozstvi);
};

#endif